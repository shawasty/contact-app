import React,{ useState, useEffect } from "react";
import {uuid} from "uuidv4";
import './App.css';
import Header from "./Header";
import Addcontact from "./Addcontact";
import ContactList from "./ContactList";



function App() {
  const LOC_STOR_KEY = "contacts";
  const [contacts,setContact] =useState([]);

  const addContactHandler = (contact) => {
    console.log(contact);
    setContact([...contacts,{id: uuid(), ...contact}])
  };
  const removeContactHandler =(id) => {
    const newContactList = contacts.filter((contact) => {
      return contact.id !== id;
    });
    setContact(newContactList);
  };

  useEffect(() => {
    const retrieveContacts =JSON.parse(localStorage.getItem(LOC_STOR_KEY));
    if(retrieveContacts) setContact(retrieveContacts);

  },[]);

  // This helps printed items not removed from the interface

  useEffect(() => {
    localStorage.setItem(LOC_STOR_KEY, JSON.stringify(contacts));

  },[contacts]);

  
  return (
    
    <div className="ui container">

      <Header />
      <Addcontact addContactHandler={ addContactHandler }/>
      <ContactList contacts={contacts} getContactId={removeContactHandler}/>
    </div>
  );
}

export default App;
